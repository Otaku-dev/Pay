# Pay

PAY  Es un script que hace el proceso de crear nuestro payload o carga útil en apk utilizando metasploit-framework. En cuestión de minutos,con una interfaz muy agradable y entendible para el usuario ,fácil de configurar y usar.


Pasos de Instalación de Pay en Termux:


Actualizamos Repositorios


apt update && apt upgrade -y


Instalamos git


apt install git -y


Instalamos las dependencias del Script


apt install figlet pv -y


Clonamos el repositorio Pay


git clone https://github.com/Fabr1x/Pay.git


Seleccionamos el fichero Pay


cd Pay


Listamos 


ls


Damos permisos de ejecución a los archivos


chmod +x *


Listamos


ls


Ejecutamos  setup.sh


bash setup.sh


Y por último ejecutamos Pay


./Pay.sh


Listo !!



Created: F@br1x and 你好😜


